package com.example.android.mobileshop;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {
    private List<Laptop> Laps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);

        rv.setHasFixedSize(true);

        initializeData();

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        LaptopAdapter adapter = new LaptopAdapter(Laps);
        rv.setAdapter(adapter);





    }

    private void initializeData() {
        Laps = new ArrayList<>();
        Laps.add(new Laptop("Lenovo Z500", "Price = 5,400 L.E", R.drawable.laptop1));
        Laps.add(new Laptop("Lenovo IP100 Ideapad", "Price = 7,800 L.E", R.drawable.laptop2));
        Laps.add(new Laptop("Lenovo IdeaPad Z50-70", "Price = 6,300 L.E", R.drawable.laptop3));
        Laps.add(new Laptop("Dell Factory Outlet", "Price = 4,900 L.E", R.drawable.laptop4));
        Laps.add(new Laptop("Dell Inspiron 5558 Core i3-4005", "Price = 6,800 L.E", R.drawable.laptop5));
        Laps.add(new Laptop("Toshiba Satellite C55-B5100 ", "Price = 6,300 L.E", R.drawable.laptop6));
        Laps.add(new Laptop("Dell Optiplex 9020 SFF", "Price = 5,800 L.E", R.drawable.laptop7));
        Laps.add(new Laptop("Aspire V ", "Price = 5,300 L.E", R.drawable.laptop8));


    }


    @Override
    public void onClick(View v) {

    }
}

