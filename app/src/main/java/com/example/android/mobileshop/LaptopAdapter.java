package com.example.android.mobileshop;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class LaptopAdapter extends RecyclerView.Adapter<LaptopAdapter.ViewHolder> {


    List<Laptop> Laps;


    LaptopAdapter(List<Laptop> Laps) {
        this.Laps = Laps;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.LaptopName.setText(Laps.get(position).name);
        holder.LaptopPrice.setText(Laps.get(position).price);
        holder.LaptopImage.setImageResource(Laps.get(position).image);
        holder.LaptopPrice.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                TextView resultTxT = (TextView) v.findViewById(R.id.Laptop_Price);
                switch (v.getId()) {

                    case R.id.Laptop_Price:
                        Toast.makeText(v.getContext(),
                                resultTxT.getText(),
                                Toast.LENGTH_LONG).show();
                        break;

                }



            }
        });
        holder.LaptopImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),
                        "This is LapTop Image",
                        Toast.LENGTH_LONG).show();

            }
        });

        holder.LaptopName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView resultTxt = (TextView) v.findViewById(R.id.Laptop_Name);

                switch (v.getId()) {

                    case R.id.Laptop_Name:
                        Toast.makeText(v.getContext(),
                                resultTxt.getText(),
                                Toast.LENGTH_LONG).show();
                        break;
                    case R.id.Laptop_Image:
                        Toast.makeText(v.getContext(),
                                "This is LapTop Name",
                                Toast.LENGTH_LONG).show();
                }

            }


        });

    }

    @Override
    public int getItemCount() {
        return Laps.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView LaptopName;
        TextView LaptopPrice;
        ImageView LaptopImage;

        ViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);
            LaptopName = (TextView) itemView.findViewById(R.id.Laptop_Name);

            LaptopPrice = (TextView) itemView.findViewById(R.id.Laptop_Price);

            LaptopImage = (ImageView) itemView.findViewById(R.id.Laptop_Image);

        }


    }
}

