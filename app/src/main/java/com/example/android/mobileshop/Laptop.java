package com.example.android.mobileshop;

public class Laptop {

    String name;
    String price;
    int image;

    Laptop(String name, String price, int image) {
        this.name = name;
        this.price = price;
        this.image = image;
    }
}
